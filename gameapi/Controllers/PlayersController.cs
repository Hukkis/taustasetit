using System.Threading.Tasks;
using gameapi.Models;
using Microsoft.AspNetCore.Mvc;
using System;

namespace gameapi.Controllers
{
    [Route("api/players")]
    public class PlayersController : Controller
    {
        [HttpGet]
        public async Task<Player[]> Get()
        {
            var players = new[]
            {
                new Player()
                {
                    Id = Guid.NewGuid(),
                    Name = "Kappa"
                },
                new Player()
                {
                    Id = Guid.NewGuid(),
                    Name = "Keepensio"
                }
            };
            return players;
        }

        [HttpPost]

        public async Task<Player> Create ([FromBody]Player player)
        {
            return player;
        }

    }
}