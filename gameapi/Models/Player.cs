using System;
namespace gameapi.Models
{
    public class Player
    {
        public Guid Id{ get; set; }
        
        public string Name{get; set;}

        public int Level{get;set;}
    }
}