using System.Threading.Tasks;
using System;
using System.IO;

namespace teht1
{
    public class OfflineCityBikeDataFetcher : ICityBikeDataFethcer
    {
        public async Task<int> GetBikeCountInStation(string stationName)
        {
            int bikeCountOnStation = -1;
            try
            {

                string[] data = await File.ReadAllLinesAsync("bikes.txt");
                for(int i = 0; i < data.Length; i++)
                {
                    if(data[i].Contains(stationName))
                    {
                        string result = System.Text.RegularExpressions.Regex.Match(data[i], @"\d+").Value;
                        bikeCountOnStation = Int32.Parse(result);
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read\n");
                Console.WriteLine(e.Message);
            }
            
            return bikeCountOnStation;

        }
    }
}