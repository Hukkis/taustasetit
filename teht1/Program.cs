﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace teht1
{
    class Program
    {
        public static HttpClient client = new HttpClient();
        public string stationName = "Tenholantie";
        public static RealTimeCityBikeDataFetcher bikedatafetcher = new RealTimeCityBikeDataFetcher();
        public static OfflineCityBikeDataFetcher offlinebikedatafetcher = new OfflineCityBikeDataFetcher();

        public static void ValidStationName(string stationName)
        {
            if(stationName.Any(char.IsDigit)){
                throw new System.ArgumentException("Name contains numbers");
            }
        }

        public static void NotFoundStationName(int returnvalue)
        {
            if(returnvalue == -1)
            {
                throw new NotFoundException("Station name not found!");
            }
        }

        public static void FetchBikeData(string stationName, bool realtime)
        {
             try
            {
                ValidStationName(stationName);
                Task<int> task;
                if(realtime)
                {
                    task = bikedatafetcher.GetBikeCountInStation(stationName);
                }
                else
                {
                    task = offlinebikedatafetcher.GetBikeCountInStation(stationName);
                }               
                task.Wait();
                NotFoundStationName(task.Result);
                Console.WriteLine(task.Result);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Error in station name: " + e.Message);
            }
            catch (NotFoundException e)
            {
                Console.WriteLine("Error in station name: " + e.Message);
            }
        }

        static void Main(string[] args)
        {
            string input;

            while(true)
            {
                Console.WriteLine("Welcome to a City Bike Fetcher.");
                Console.WriteLine("This program will tell you the number of bikes available on searched station.");
                Console.WriteLine("Type 'exit' to quit");
                Console.WriteLine("Insert a Station name to be searched: ");
                input = Console.ReadLine();

                if(input == "exit")
                {
                    break;
                }

                Console.WriteLine("\n Would you like to search Online or Offline?\n");
                string of = Console.ReadLine();
                bool searchbool;

                if (of == "Online" || of == "online")
                {
                    searchbool = true;
                }
                else
                {
                    searchbool = false;
                }

                FetchBikeData(input,searchbool);
            }           
        }
    }
}
