using System.Threading.Tasks;

using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace teht1
{
    public interface ICityBikeDataFethcer
    {
         Task<int> GetBikeCountInStation(string stationName);
    }
}