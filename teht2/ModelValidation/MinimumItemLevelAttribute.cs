using System.ComponentModel.DataAnnotations;
using teht2.Model;

namespace teht2.ModelValidation
{
    public class MinimumItemLevelAttribute : ValidationAttribute
    {
        
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            NewItem item = (NewItem)validationContext.ObjectInstance;

            if(item.Type.Equals("sword") && item.Level < 3)
            {
                return new ValidationResult("The level requirement for 'sword' is 3!\n" );
            }
            return ValidationResult.Success;
        }

    }
}