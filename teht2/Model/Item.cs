using System;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson.Serialization.Attributes;

namespace teht2.Model
{
    public class Item
    {
        [BsonId]
        public Guid Id {get;set;}
        public string Name {get;set;}
        public string Type{get;set;}
        [Range(1,20)]
        public int Level{get;set;}
        public int price{get;set;}
        public DateTime CreationDate {get;set;}

    }
}