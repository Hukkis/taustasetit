using System;
using System.ComponentModel.DataAnnotations;
using teht2.ModelValidation;

namespace teht2.Model
{
    public class NewItem
    {
        [Required]
        public string Name {get;set;}
        [Required]
        public string Type{get;set;}
        [MinimumItemLevel]
        [Range(1,20)]
        public int Level{get;set;}
        public int? price{get;set;}
        public DateTime CreationDate {get;set;}
    }
}