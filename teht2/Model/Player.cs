using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace teht2.Model
{
    public class Player
    {

        [BsonId]
        public Guid Id {get; set;}
        public string Name {get; set;}
        public int Level {get; set;}
        public int Points {get;set;}
        public string Clan {get;set;}
        public List<Item> Items {get;set;}
    }
}