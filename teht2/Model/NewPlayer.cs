using System.ComponentModel.DataAnnotations;

namespace teht2.Model
{
    public class NewPlayer
    {
        [Required]
        public string Name{get;set;}
    }
}