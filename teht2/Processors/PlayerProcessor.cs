using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using teht2.Model;
using teht2.Repositories;

namespace teht2.Processors
{
    public class PlayerProcessor
    {
        private readonly IPlayersRepository _repository;

        public PlayerProcessor(IPlayersRepository repository)
        {
            _repository = repository;
        }
        public Task<Player[]> GetAll()
        {
            return _repository.GetAll();
        }

        public Task<Player[]> GetAllByInventory(int size)
        {
            return _repository.GetAllByInventory(size);
        }

        public Task<Player[]> GetAllByProperty(string property)
        {
            return _repository.GetAllByProperty(property);
        }
        public Task<Player> Get(Guid id)
        {
            return _repository.Get(id);
        }

        public Task<Player[]> GetAllByPoints(int points)
        {
            return _repository.GetAllByPoints(points);
        }

        public Task<Player> GetByName(string name)
        {
            return _repository.GetByName(name);
        }

        public Task<Player> Create(NewPlayer player)
        {
            Player _player = new Player()
            {
                Id = Guid.NewGuid(),
                Name = player.Name,
                Level = 1,
                Clan = "",
                Items = new List<Item>()
            };
            return _repository.Create(_player);
        }

        public Task<Player> Modify(Guid id, ModifiedPlayer player)
        {
            Player _player = new Player();
            return _repository.Modify(id,_player);
        }

        public Task<Player> AddScore(Guid id, int score)
        {
            return _repository.AddScore(id,score);
        }

        public Task<Player> Delete(Guid id)
        {
            return _repository.Delete(id);
        }

        public Task<int> GetCommonPlayerLevel()
        {
            return _repository.GetCommonPlayerLevel();
        }
    }
}