using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using teht2.Exceptions;
using teht2.Model;
using teht2.Repositories;

namespace teht2.Processors
{
    public class ItemsProcessor
    {
        private readonly IPlayersRepository _repository;

        public ItemsProcessor(IPlayersRepository repository)
        {
            _repository = repository;
        }

        public async Task<Item> GetItem(Guid playerId,Guid itemId)
        {
            return await _repository.GetItem(playerId, itemId);
        }

        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            return await _repository.GetAllItems(playerId);
        }

        public async Task<Item> CreateItem(Guid playerId, NewItem newitem)
        {

            Player player = await _repository.Get(playerId);
            Random rand = new Random();
            Item createditem = new Item()
            {
                Id = Guid.NewGuid(),
                Name = newitem.Name,
                Type = newitem.Type,
                Level = newitem.Level,
                price = rand.Next(0,200),
                CreationDate = DateTime.Today
            };

            if(player.Level < createditem.Level)
            {
                throw new NotHighLevelEnoughException();
            }

            return await _repository.CreateItem(playerId,createditem);
        }

        public async Task<Item> ModifyItem(Guid playerId, Guid itemId, ModifiedItem item)
        { 
            Item modifieditem = await _repository.GetItem(playerId,itemId);
            modifieditem.Name = item.Name; 
            return await _repository.ModifyItem(playerId,itemId,modifieditem);
        }

        public async Task<Item> DeleteItem(Guid playerId, Guid itemId)
        {
            Player player = await _repository.Get(playerId);
            
            return await _repository.DeleteItem(playerId,itemId);
        }

        public async Task<Player> SellItem(Guid id, int amount)
        {
            return await _repository.SellItem(id,amount);
        }
    }
}