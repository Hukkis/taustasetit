using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System;
using teht2.Model;
using teht2.Processors;
using teht2.ModelValidation;
using teht2.Exceptions;

namespace teht2.Controllers
{

    [Route("api/players")]

    public class PlayerController : Controller
    {    
        private PlayerProcessor _processor;
        public PlayerController(PlayerProcessor processor)
        {
            _processor = processor;
        }

        [HttpGet]
        [HttpGet("inventorysize/{size:int}")]
        [HttpGet("points/{points:int}")]
        [HttpGet("{property}")]
        public async Task<Player[]> GetAll(int? size, int? points, string property)
        {
            if(size != null)
                return await _processor.GetAllByInventory((int)size);
            else if(property != null)
                return await _processor.GetAllByProperty(property);
            else if(points != null)
                return await _processor.GetAllByPoints((int)points);
            else
                return await _processor.GetAll();    
                
        }


        [HttpGet("{id:Guid}")]       
        [HttpGet("{name}")] 
        public async Task<Player> Get(Guid id, string name)
        {
            if(name == null)
                return await _processor.Get(id);  
            else
                return await _processor.GetByName(name);
        }

        [HttpPost]
        [ValidateModel]
        public async Task<Player> Create([FromBody]NewPlayer newplayer)
        {
            return await _processor.Create(newplayer);
        }

        [HttpPost("{id:Guid}")]
        public async Task<Player> Modify(Guid id,[FromBody]ModifiedPlayer modifiedPlayer)
        {
            return await _processor.Modify(id, modifiedPlayer);
        }

        [HttpPost("{id:Guid}/addscore/{score:int}")]
        public async Task<Player> AddScore(Guid id,int score)
        {
            return await _processor.AddScore(id,score);
        }

        [HttpDelete("{id:Guid}")]
        public async Task<Player> Delete(Guid id)
        {
            return await _processor.Delete(id);
        }

        [HttpGet("{commonlevel}")]
        public async Task<int> GetCommonPlayerLevel()
        {
            return await _processor.GetCommonPlayerLevel();
        }

    }
}