using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System;
using teht2.Model;
using teht2.Processors;
using teht2.ModelValidation;

namespace teht2.Controllers
{
    [Route("api/players/{playerId}/items")]
    public class ItemController : Controller
    {
        private ItemsProcessor _processor;

        public ItemController(ItemsProcessor processor)
        {
            _processor = processor;
        }

        [HttpGet]
        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            return await _processor.GetAllItems(playerId);
        }

        [HttpGet("{itemId:Guid}")]
        public async Task<Item> GetItem(Guid playerId, Guid itemId)
        {
            return await _processor.GetItem(playerId,itemId);
        }

        [HttpPost]
        [ValidateModel]
        public async Task<Item> CreateItem(Guid playerId, [FromBody]NewItem newitem)
        {
            return await _processor.CreateItem(playerId, newitem);
        }

        [HttpPost("{itemId:Guid}")]
        public async Task<Item> ModifyItem(Guid playerId, Guid itemId, [FromBody] ModifiedItem modifieditem)
        {
            return await _processor.ModifyItem(playerId, itemId, modifieditem);
        }

        [HttpDelete("{itemId:Guid}")]
        public async Task<Item> DeleteItem(Guid playerId, Guid itemId)
        {
            return await _processor.DeleteItem(playerId, itemId);
        }

        [HttpDelete("{itemId:Guid}/sell/{amount:int}")]
        public async Task<Player> SellItem(Guid id, int amount)
        {
            return await _processor.SellItem(id,amount);
        }
        
    }
}