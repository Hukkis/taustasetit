using System;
using System.Threading.Tasks;
using teht2.Model;
namespace teht2.Repositories
{
    public interface IPlayersRepository
    {
        Task<Player> Get(Guid id);

        Task<Player> GetByName(string name);
        Task<Player[]> GetAll();
        Task<Player[]> GetAllByInventory(int size);
        Task<Player[]> GetAllByProperty(string property);
        Task<Player[]> GetAllByPoints(int points);
        Task<Player> Create(Player player);
        Task<Player> Modify(Guid id, Player player);
        Task<Player> AddScore(Guid id, int score);
        Task<Player> Delete(Guid id);
        Task<Player> SellItem(Guid id, int amount);
        Task<int> GetCommonPlayerLevel();

        Task<Item> GetItem(Guid playerId, Guid itemId);
        Task<Item[]> GetAllItems(Guid playerId);
        Task<Item> CreateItem(Guid playerId, Item item);
        Task<Item> ModifyItem(Guid playerId, Guid itemId, Item item);
        Task<Item> DeleteItem(Guid playerId, Guid itemId);
    }
}