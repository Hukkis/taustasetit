using teht2.Mongodb;
using MongoDB.Driver;
using teht2.Model;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Bson;

namespace teht2.Repositories
{
    public class MongoDbRepository : IPlayersRepository
    {
        private IMongoCollection<Player> _collection;
        private IMongoDatabase _database;
        public MongoDbRepository(MongoDBClient client)
        {
            _database = client.GetDatabase("game");
            _collection = _database.GetCollection<Player>("players");
        }

        public async Task<Player> Create(Player player)
        {
            await _collection.InsertOneAsync(player);
            return player;
        }

        public async Task<Item> CreateItem(Guid playerId, Item item)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Id, playerId);
            var update = Builders<Player>.Update.Push(p => p.Items, item);
            await _collection.FindOneAndUpdateAsync(filter,update);
            return item;
        }

        public async Task<Player> Delete(Guid id)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Id, id);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();

            await _collection.DeleteOneAsync(filter);
            return player;
        }

        public async Task<Item> DeleteItem(Guid playerId, Guid itemId)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Id, playerId);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();

            List<Item> items = player.Items;
            Item deleteditem = null;

            for(int i = 0; i<items.Count; i++)
            {
                if(items[i].Id.Equals(itemId))
                {
                    deleteditem = items[i];
                }
            }
            items.Remove(deleteditem);
            player.Items = items;
            
            await _collection.ReplaceOneAsync(filter,player);
            return deleteditem;
        }

        public async Task<Player> SellItem(Guid id, int amount)
        {
            var filter = Builders<Player>.Filter.ElemMatch(p => p.Items, i => i.Id == id);
            var update = Builders<Player>.Update.Inc(p => p.Points, amount)
            .PullFilter(p => p.Items, i => i.Id == id);

            return await _collection.FindOneAndUpdateAsync(filter,update);
        }

        public async Task<Player> Get(Guid id)
        {
             var filter = Builders<Player>.Filter.Eq(p => p.Id, id);
             var cursor = await _collection.FindAsync(filter);
             var player = await cursor.FirstAsync();
             return player;
        }

        public async Task<Player> GetByName(string name)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Name, name);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();
            return player;
        }

        public async Task<Player[]> GetAll()
        {
            var list = await _collection.Find(_ => true).ToListAsync();
            return list.ToArray();
        }

        public async Task<Player[]> GetAllByProperty(string property)
        {
            var filter = Builders<Player>.Filter.ElemMatch(p => p.Items, i => i.Type == property);
            var list = await _collection.Find(filter).ToListAsync();
            return list.ToArray();
        }
        public async Task<Player[]> GetAllByInventory(int size)
        {
            var filter = Builders<Player>.Filter.Size(p => p.Items, size);
            var list = await _collection.Find(filter).ToListAsync();
            return list.ToArray();
        }

        public async Task<Player[]> GetAllByPoints(int points)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Points, points);
            var list = await _collection.Find(filter).ToListAsync();
            return list.ToArray();
        }

        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Id, playerId);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();
            return player.Items.ToArray();
        }

        public async Task<Item> GetItem(Guid playerId, Guid itemId)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Id, playerId);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();

            for(int i = 0; i < player.Items.Count; i++)
            {
                if(player.Items[i].Equals(itemId))
                {
                    return player.Items[i];
                }
            }
            return null;
        }

        public async Task<Player> Modify(Guid id, Player player)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Id, id);
            var update = Builders<Player>.Update.Set(p => p.Name, player.Name)
            .Set(p => p.Clan, player.Clan)
            .Set(p => p.Level, player.Level);
            return await _collection.FindOneAndUpdateAsync(filter, update);
        }

        public async Task<Player> AddScore(Guid id, int score)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Id,id);
            var update = Builders<Player>.Update.Inc(p => p.Points, score);
            return await _collection.FindOneAndUpdateAsync(filter,update); 
        }

        public async Task<Item> ModifyItem(Guid playerId, Guid itemId, Item item)
        {

            // search player who has item.

            var filter = Builders<Player>.Filter.And(Builders<Player>.Filter.Eq(p => p.Id, playerId),
            Builders<Player>.Filter.ElemMatch(p => p.Items, i => i == item));

            var update = Builders<Player>.Update.Set(p => p.Items[-1], item);
            await _collection.FindOneAndUpdateAsync(filter,update);
            return item;
        }

        public async Task<int> GetCommonPlayerLevel()
        {
            var collection = _database.GetCollection<BsonDocument>("players");
            var aggregate = _collection.Aggregate().Project(new BsonDocument { {"Level", 1}})
            .Group(new BsonDocument { {"_id", "$Level"},{ "Count", new BsonDocument("$sum",1)}})
            .Sort(new BsonDocument { { "Count", -1}})
            .Limit(1);

            BsonDocument result = await aggregate.FirstAsync();
            return result[0].AsInt32;
        }
    }
}