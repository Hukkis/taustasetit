/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using teht2.Exceptions;
using teht2.Model;

namespace teht2.Repositories
{
    public class PlayersInMemoryRepository : IPlayersRepository
    {
        private Dictionary<Guid, Player> _players = new Dictionary<Guid, Player>();
        
        public async Task<Player[]> GetAll()
        {
            return _players.Values.ToArray();
        }

        public async Task<Player> Get(Guid id)
        {
            if(_players.ContainsKey(id) == false)
            {
                throw new NotFoundException();
            }
            return _players[id];
        }

        public async Task<Player> Create(Player player)
        {
            _players.Add(player.Id, player);
            return player;
        }

        public async Task<Player> Modify(Guid id, Player player)
        {
            _players[id] = player;
            return player;
        }

        public async Task<Player> Delete(Guid id)
        {
            Player deleted = _players[id];
            _players.Remove(id);
            return deleted;
        }

        public async Task<Item> GetItem(Guid playerId, Guid itemId)
        {
            if(_players.ContainsKey(playerId) == false)
            {
                throw new NotFoundException();
            }
            return _players[playerId].GetItem(itemId);
        }

        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            if(_players.ContainsKey(playerId) == false)
            {
                throw new NotFoundException();
            }
            return _players[playerId].GetItems().Values.ToArray();
        }

        public async Task<Item> CreateItem(Guid playerId, Item item)
        {
            if(_players.ContainsKey(playerId) == false)
            {
                throw new NotFoundException();
            }

            _players[playerId].AddItem(item.Id, item);
            return item;
        }

        public async Task<Item> ModifyItem(Guid playerId, Guid itemId, Item item)
        {
            Player player = _players[playerId];
            Item[] items = player.Items;
            Item tempitem = null;

            for(int i = 0; i<items.Length; i++)
            {
                if(items[i].Id.Equals(itemId))
                {
                    items[i] = item;
                    tempitem = item;
                }
            }
            return item;
        }

        public async Task<Item> DeleteItem(Guid playerId, Guid itemId)
        {
            Item deleteditem = _players[playerId].GetItem(itemId);
            _players[playerId].GetItems().Remove(itemId);
            return deleteditem;
        }



    }
}*/ // previous stuff.