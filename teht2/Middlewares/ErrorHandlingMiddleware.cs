using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using teht2.Exceptions;

namespace teht2.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private RequestDelegate _next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            bool failed = false;
            bool nothighlevelenough = false;
            try
            {
                await _next(context);
            }
            catch(NotFoundException e)
            {
                context.Response.StatusCode = 404;
                failed = true;
            }
            catch(NotHighLevelEnoughException e)
            {
                context.Response.StatusCode = 406;
                nothighlevelenough = true;                
            }

            if(failed == true)
            {
                await context.Response.WriteAsync("Could not find the player");
            }
            if(nothighlevelenough)
            {
                await context.Response.WriteAsync("Player level not hight enough :<");
            }
        }
    }
}